import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { AuthProvider } from "./contexts/AuthContext";
import Login from "./routes/Login";
import PrivateRoute from "./routes/PrivateRoute";
import Router from "./routes/Router";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "./index.css";
// import './src/fonts/ProductSans-Black.ttf'; 
import "./fonts/ProductSans-Black.ttf"; 

function App() {
  return (
    <BrowserRouter>
     <AuthProvider>
        <Switch>
          <Route path="/login" component={Login} />
          {console.log("init out")}
          <PrivateRoute path="/" component={Router} />
        </Switch>
      </AuthProvider>
    </BrowserRouter>
  );
}

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
