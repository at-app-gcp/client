import {
  Row,
  Col,
  Container,
  Card,
  CardColumns,
  Table,
  Modal,
  Button,
  Accordion,
} from "react-bootstrap";
// Get billing / project cost info from cloud fx => bash etc
import React, { useState } from "react";
import { Switch, Route, Link, useHistory } from "react-router-dom";
import { useAuth } from "../../contexts/AuthContext";

export default function Settings(props: any) {
  const [error, setError] = useState("");
  const { currentUser, logout } = useAuth();
  const history = useHistory();

  async function handleLogout() {
    setError("");

    try {
      await logout();
      history.push("/login");
    } catch {
      setError("Failed to log out");
    }
  }
  const sessionStart = new Date(currentUser.metadata.lastSignInTime).getTime();
  const sessionEnd = new Date().getTime();

  const result = sessionEnd - sessionStart;
  const resultGT = new Date(result).getTime();

  console.log(resultGT);
  return (
    <>
      <div>
        <Accordion style={{ paddingTop: "50vh" }}>
          <Card className="bg-dark text-white" style={{ border: "none" }}>
            <Card.Header
              style={{ padding: "0px 0px 5px 0px", textAlign: "center", borderRadius :"0px",
              borderBottomLeftRadius : "50px 20px !important"}}
            >
              <Accordion.Toggle
                as={Button}
                style={{borderRadius : "0px"}}
                variant="light"
                eventKey="0"
                size="md"
                block
              >
                User Details
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="0">
              <Card.Body style={{ textAlign: "center" }}>
                <div>
                  <div>
                    Logged user : {currentUser.email}{" "}
                  </div>
                  <div>
                    User created : {currentUser.metadata.creationTime}
                  </div>
                  <div>
                    Last Sign in : {currentUser.metadata.lastSignInTime}
                  </div>
                  <div>
                    GCP project  : {currentUser.o}
                  </div>
                </div>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
          <Card className="bg-dark text-white" style={{ border: "none" }}>
            <Card.Header
              style={{ padding: "5px 0px 0px 0px", textAlign: "center" }}
            >
              <Accordion.Toggle
                as={Button}
                variant="danger"
                onClick={handleLogout}
                eventKey="1"
                size="md"
                block
                style={{ border: "none",borderRadius : "0px" }}
              >
                Log Out
              </Accordion.Toggle>
            </Card.Header>
          </Card>
        </Accordion>
      </div>

      {/* 
      <Container style={{ fontFamily: "roboto ", textAlign: "center", padding :"25px 0px 0px 0px" }}>
        <div>
          <b>Logged user</b> : {currentUser.email}
        </div>
        <div>User created : {currentUser.metadata.creationTime}</div>
        <div>Last Sign in : {currentUser.metadata.lastSignInTime}</div>
        <div>GCP project : {currentUser.o}</div>
        <Button variant="outline-danger" onClick={handleLogout}>
          Log out
        </Button>
      </Container> */}
    </>
  );
}
