import React, { useEffect, useState, Component } from "react";
import { firestore } from "../../../../routes/Firebase";
import { FcOk, FcHighPriority } from "react-icons/fc";
import {
  Row,
  Col,
  Container,
  Card,
  CardColumns,
  Table,
  Spinner,
} from "react-bootstrap";

interface ISystemCard {
  name: string;
}

type State = {
  name?: string;
  uid?: string;
  inception?: Object | null | string;
  date?: string | null;
  payload: any | null;
};
export default class SystemCard extends Component<ISystemCard, State> {
  state: State = {
    // name: "",
    // uid: "",
    // inception: null,
    // date: null,
    payload: null,
  };

  apiQuery = firestore.collection("SYSTEM").doc("server");

  componentDidMount() {
    this.apiQuery.get().then((res) => {
      this.setState(() => ({
        payload: res.data(),
      }));
      // console.log(this.state.payload, "state");
    });
  }

  render() {
    return (
      <>
        <Card className="mb-2" >
          <Card.Header as="h5">System</Card.Header>
          <Card.Body>
            <Card.Text>
              <Table>
                <tbody>
                  {this.state.payload ? (
                    <div>
                      {/* {Object.keys(this.state.payload).map((e, i, s) => {
                        console.log(typeof this.state.payload[e] , "state");
                        
                        return (
                          <tr key={i}>
                            <td>{e}</td>
                            <td>{this.state.payload[e]}</td>
                          </tr>
                        );
                      })} */}
                      {/* {console.log(this.state.payload)} */}
                    </div>
                  ) : (
                    <div style={{ textAlign: "center" }}>
                      <Spinner animation="border" as={"p"} />
                    </div>
                  )}
                </tbody>
              </Table>
              {/* {this.state.payload ? 
                  
                  (<div>
                      {
                          Object.keys(this.state.payload).map((e,i,s) => {
                              return (<div>{(e)} :|: {this.state.payload[e]}</div>)
                              return (
                                <tr>
                                    <td>{(e)}</td>
                                    <td>{this.state.payload[e]}</td>
                                </tr>
                              )    
                          })
                      }
                  </div>)
                  :
                    (<div style={{textAlign : "center"}}><Spinner animation="border" as={"p"} /></div> )
                  } */}
            </Card.Text>
          </Card.Body>
        </Card>
      </>
    );
  }
}

// import React, { useEffect, useState, Component } from "react";
// import { firestore } from "../../../../routes/Firebase";
// import { FcOk, FcHighPriority } from "react-icons/fc";
// import { Row, Col, Container, Card, CardColumns, Table } from "react-bootstrap";

// interface IHomeCard {
//   name: string;
// }
// // [SYSTEM,"INIT",
// //         "FINISH"
// // [ERRORS]
// // [CLOUD]
// type State = {
//   name?: string;
//   uid?: string;
//   inception?: Object | null | string;
//   date?: string | null;
//   payload: any | null;
// };
// export default class HomeCard extends Component<IHomeCard, State> {
//   state: State = {
//     // name: "",
//     // uid: "",
//     // inception: null,
//     // date: null,
//     payload: null,
//   };

//   apiQuery = firestore.collection("GLOBAL").doc("INIT");

//   componentDidMount() {
//     this.apiQuery.get().then((res) => {
//       this.setState(() => ({
//         payload: res.data(),
//       }));
//       console.log(this.state, "state");
//     });
//   }

//   render() {
//     return (
//       <>
//         {this.state.payload ? (
//           <Card border={"success"} className="mb-2" style={{ width: "100%" }}>
//             <Card.Header as="h5">{this.props.name}</Card.Header>
//             <Card.Body>
//               <Card.Text>
//                 {this.state.payload.payload.instruments.map((e: any) => {
//                   return <div>{e}</div>;
//                 })}{" "}
//                 <br />
//                 {this.state.payload.payload.time_frames.map((e: any) => {
//                   return <div>{e}</div>;
//                 })}{" "}
//                 <br />
//                 {this.state.payload.payload.loop_active.toString()} <br />
//                 {this.state.payload.payload.loop_counter} <br />
//                 {this.state.payload.payload.workers_count} <br />
//                 {this.state.payload.payload.loop_termination} <br />
//                 {this.state.payload.payload.program_start_time
//                   .toDate()
//                   .toString()}{" "}
//                 <br />
//                 {this.state.payload.payload.system_info.boot_time} <br />
//                 <br />
//                 {this.state.payload.payload.system_info.cpu} <br />
//                 {this.state.payload.payload.system_info.cpu_count} <br />
//                 {Object.keys(this.state.payload.payload.system_info).map(
//                   (e) => {
//                     return (
//                       <div>
//                         {e}{" "}
//                         {/* {this.state.payload.payload.system_info[e].map(
//                           (ei: any) => {
//                             return <div>{ei}</div>;
//                           }
//                         )} */}
//                       </div>
//                     );
//                   }
//                 )}{" "}
//                 <br />
//                 {this.state.payload.payload.system_info.cpu_freq.map(
//                   (e: any) => {
//                     return <div>{e}</div>;
//                   }
//                 )}{" "}
//                 <br />
//                 {this.state.payload.payload.system_info.cpu_stats.map(
//                   (e: any) => {
//                     return <div>{e}</div>;
//                   }
//                 )}{" "}
//                 <br />
//                 {this.state.payload.payload.system_info.cpu_times.map(
//                   (e: any) => {
//                     return <div>{e}</div>;
//                   }
//                 )}{" "}
//                 {this.state.payload.payload.system_info.cpu_usage}asd <br />{" "}
//                 <br />
//                 {this.state.payload.payload.system_info.disk.map((e: any) => {
//                   return <div>{e}</div>;
//                 })}{" "}
//                 {this.state.payload.payload.system_info.disk_bytes.map(
//                   (e: any) => {
//                     return <div>{e}</div>;
//                   }
//                 )}{" "}
//                 {this.state.payload.payload.system_info.disk_io.map(
//                   (e: any) => {
//                     return <div>{e}</div>;
//                   }
//                 )}{" "}
//                 <br /> <br />
//                 {this.state.payload.payload.system_info.load_avg.map(
//                   (e: any) => {
//                     return <div>{e}</div>;
//                   }
//                 )}{" "}
//                 <br /> <br />
//                 {this.state.payload.payload.system_info.memory.available} <br />
//                 {this.state.payload.payload.system_info.memory.free} <br />
//                 {this.state.payload.payload.system_info.memory.percent} <br />
//                 {this.state.payload.payload.system_info.memory.total} <br />
//                 {this.state.payload.payload.system_info.memory_free}
//                 <br />
//                 {this.state.payload.payload.system_info.memory_swap.map(
//                   (e: any) => {
//                     return <div>{e}</div>;
//                   }
//                 )}{" "}
//                 {this.state.payload.payload.system_info.memory_used}
//                 <br />
//                 <br />
//                 {this.state.payload.payload.system_info.net_conn_len}sad <br />
//                 {this.state.payload.payload.system_info.net_io_.map(
//                   (e: any) => {
//                     return <div>{e}</div>;
//                   }
//                 )}{" "}
//                 <br />
//                 {this.state.payload.payload.system_info.platform.map(
//                   (e: any) => {
//                     return <div>{e}</div>;
//                   }
//                 )}
//                 {this.state.payload.payload.system_info.processes_len} <br />
//                 {this.state.payload.payload.system_info.user.map((e: any) => {
//                   return <div>{e}</div>;
//                 })}{" "}
//               </Card.Text>
//             </Card.Body>
//           </Card>
//         ) : null}
//       </>
//     );
//   }
// }
