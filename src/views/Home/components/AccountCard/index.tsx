import React, { useEffect, useState, Component } from "react";
import { firestore } from "../../../../routes/Firebase";
import { FcOk, FcHighPriority } from "react-icons/fc";
import {
  Row,
  Col,
  Container,
  Card,
  CardColumns,
  Table,
  Spinner,
} from "react-bootstrap";

interface IAccountCard {
  name: string;
}

type State = {
  name?: string;
  uid?: string;
  inception?: Object | null | string;
  date?: string | null;
  payload: any | null;
};
export default class AccountCard extends Component<IAccountCard, State> {
  state: State = {
    // name: "",
    // uid: "",
    // inception: null,
    // date: null,
    payload: null,
  };

  apiQuery = firestore.collection("ACCOUNT").doc("oanda_practice");

  componentDidMount() {
    this.apiQuery.get().then((res) => {
      this.setState(() => ({
        payload: res.data(),
      }));
      console.log(this.state.payload, "state");
    });
  }

// : "40025"
// marginCallMarginUsed: "26372.3939"
// : "0.32446"
// marginCloseoutMarginUsed: "26372.3939"
// marginCloseoutNAV: "81279.7579"
// marginCloseoutPercent: "0.16223"
// marginCloseoutPositionValue: "639679.6507"
// marginCloseoutUnrealizedPL: "216.6549"
// marginRate: "0.02"
// marginUsed: "26372.3939"




  render() {
    return (
      <>
        {this.state.payload ? (
          <Table striped bordered hover variant="dark">

            <tbody>

              <tr>
                <td>Balance</td>
                <td>{Math.round(this.state.payload.balance)}   {" "}  {this.state.payload.currency} </td>
              </tr>
              <tr>
                <td>Position Value</td>
                <td>{Math.round(this.state.payload.positionValue)}   {" "}  {this.state.payload.currency} </td>
              </tr>
              
              <tr>
                <td>Open Trades</td>
                <td>{Math.round(this.state.payload.openTradeCount)}</td>
              </tr>
              <tr>
                <td>Open Positions</td>
                <td>{Math.round(this.state.payload.openPositionCount)}</td>
              </tr>
              
              <tr>
                <td>P/L</td>
                <td>{Math.round(this.state.payload.pl)}  {" "}  {this.state.payload.currency} </td>
              </tr>
              <tr>
                <td>Unrealised P/L</td>
                <td>{Math.round(this.state.payload.unrealizedPL)}  {" "}  {this.state.payload.currency} </td>
              </tr>
              
              <tr>
                <td>Account ID</td>
                <td>{this.state.payload.id}</td>
              </tr>
              <tr>
                <td>Margin Available </td>
                <td>{Math.round(this.state.payload.marginAvailable)}  {" "}  {this.state.payload.currency} </td>
              </tr>
              <tr>
                <td>Margin Used </td>
                <td>{Math.round(this.state.payload.marginUsed)}  {" "}  {this.state.payload.currency} </td>
              </tr>
              
              <tr>
                <td>Used Margin Call</td>
                <td>{Math.round(this.state.payload.marginCallMarginUsed)}  {" "}  {this.state.payload.currency} </td>
              </tr>
              <tr>
                <td>Margin Call %</td>
                <td>{this.state.payload.marginCallPercent}  {" "}  % </td>
              </tr>

              
              <tr>
                <td>Pending Orders</td>
                <td>{Math.round(this.state.payload.pendingOrderCount)}</td>
              </tr>
              <tr>
                <td>Pending Orders</td>
                <td>{Math.round(this.state.payload.lastTransactionID)}</td>
              </tr>
              <tr>
                <td>NAV</td>
                <td>{Math.round(this.state.payload.NAV)}   {" "}  {this.state.payload.currency} </td>
              </tr>
              

            </tbody>
          </Table>
        ) : (
          // <Table
          //   striped
          //   bordered
          //   hover
          //   size="sm"
          //   responsive="sm"
          //   variant="dark"
          // >
          //   <tbody>
          //     <div>
          //       {Object.keys(this.state.payload).filter((e, i, s) => {
          //         console.log(this.state.payload, "payload");
          //         // return (
          //         //   <div>
          //         //     <div style={{}}>{e} </div> : <div>{this.state.payload[e]}</div>
          //         //   </div>
          //         // );
          //         return (
          //           <tr key={i}>
          //             <td>{e}</td>
          //             <td>{this.state.payload[e]}</td>
          //           </tr>
          //         );
          //       })}
          //     </div>
          //   </tbody>
          // </Table>
          <div style={{ textAlign: "center" }}>
            <Spinner animation="border" as={"p"} />
          </div>
        )}
      </>
    );
  }
}
