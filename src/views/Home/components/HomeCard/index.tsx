import React, { useEffect, useState, Component } from "react";
import { firestore } from "../../../../routes/Firebase";
import { FcOk, FcHighPriority } from "react-icons/fc";
import { Row, Col, Container, Card, CardColumns, Table } from "react-bootstrap";

interface IHomeCard {
  name : string;
  key: number;
  query : string
}
// [SYSTEM,"INIT",
//         "FINISH"
// [ERRORS] 
// [CLOUD] 
type State = {
  name?: string;
  uid?: string;
  inception?: Object | null | string;
  date?: string | null;
  payload : any | null
};
export default class HomeCard extends Component<IHomeCard, State> {
  state: State = {
    // name: "",
    // uid: "",
    // inception: null,
    // date: null,
    payload : null
  };

  apiQuery = firestore
    .collection("GLOBAL")
    .doc(this.props.query)

  componentDidMount() {
    this.apiQuery.get().then((res) => {
      this.setState(() => ({
        payload :res.data()
      }))
        console.log(this.state, 'state')
        
    })
  }

//   componentDidUpdate(prevProps: any, prevState: any) {
//     console.log("componentDidUpdate: ", prevProps, prevState.date);
//     if (prevState.date !== this.state.date) {
//       this.apiQuery.onSnapshot((snap) => {
//         this.setState(() => ({
//           date: snap.data()?.payload.date.toDate().toString(),
//         }));
//         // console.log(snap.data()?.payload.date.toDate().toString(), 'kk')
//         console.log(this.state.date, "date");
//         console.log(prevState.date, " prevsTate.date");
//       });

//       // return () => this.unsubscribe()
//     }
//   }
  alfa = () => Object.keys(this.state.payload.payload)
  // console.log(alfa())
  render() {
    return (
      <>
      {this.state.payload ? 
      (
       <Row>
         <Card
           border={"success"}
           key={this.props.key}
           className="mb-2"
           style={{ width: "100%" }}
         >
           <Card.Header as="h5">{this.props.name}</Card.Header>
           <Card.Body>
             <Card.Text >
               {/* {this.state.payload.payload.instruments} */}
               {Object.keys(this.state.payload.payload).map((e,i) => {
                 return (
                   <div>
                  {typeof this.state.payload.payload[e] !== "object"  ? this.state.payload.payload[e] : console.log(typeof this.state.payload.payload[e], 'obj') }
                   </div>
                 )
               })}
             </Card.Text>
           </Card.Body>
         </Card>
       </Row>
      )
      : null
    } 

      </>

    );
  }
}
