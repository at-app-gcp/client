import React, { Component } from "react";
import { firestore } from "../../routes/Firebase";
import { Table } from "react-bootstrap";
import {
  Formik,
  FormikHelpers,
  FormikProps,
  Form,
  Field,
  FieldProps,
} from 'formik';

interface MyFormValues {
  firstName: string;
}

const Inf: React.FC<{}> = () => {
   const initialValues: MyFormValues = { firstName: '' };
   return (
     <div>
       <h1>My Example</h1>
       <Formik
         initialValues={initialValues}
         onSubmit={(values, actions) => {
           console.log({ values, actions });
           alert(JSON.stringify(values, null, 2));
           actions.setSubmitting(false);
         }}
       >
         <Form>
           <label htmlFor="firstName">First Name</label>
           <Field id="firstName" name="firstName" placeholder="First Name" />
           <button type="submit">Submit</button>
         </Form>
       </Formik>
     </div>
   );
 };

function Error({ todo, index, removeTodo }: any) {
  const data = todo.data()
  console.log(data, 'todo here')
  return (
    <tr>
      <td>{data.workerID}</td>
      <td>{data.message}</td>
      <td>{data.time}</td>
      <td><button onClick={() => removeTodo(index)}>x</button></td>
    </tr>

  );
}


type State = {
  errors: Array<{}> | null;
};

export default class Errors extends Component {


  apiQuery = firestore.collection("ERRORS");
  state: State = {
    errors: null,
  };

  componentDidMount() {
    this.apiQuery.onSnapshot(snap => {
      this.setState({ errors: snap.docs })
      console.log(snap.docs, " did mount")
    })

  }

  removeTodo = (index: any) => {
    const newTodos = this.state.errors
    newTodos?.splice(index, 1);
    this.setState({ errors: newTodos });
  };

  render() {
    return (
      <Table striped bordered hover size="sm" responsive="sm" variant="dark">
        <thead>
          <tr>
            <th className="not_bold" >        <Inf/></th>
            <th className="not_bold" >Message</th>
            <th className="not_bold" >Time</th>
            <th className="not_bold" >Dismiss</th>
          </tr>
        </thead>
        <tbody>
          {this.state.errors?.length === 0 ?
            <div>No errors </div>
            :
            this.state.errors?.map((todo, index) => (
              <Error
                key={index}
                index={index}
                todo={todo}
                removeTodo={this.removeTodo}
              />
            ))}
        </tbody>
      </Table>
    )
  }
}
