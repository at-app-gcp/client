import React, { useEffect, useState, Component } from "react";
import { firestore } from "../../../../routes/Firebase";
import { FcOk, FcHighPriority } from "react-icons/fc";
import { MdExplicit } from 'react-icons/md'

interface IWorker {
  instrument: string;
  timeFrame: string;
}

type State = {
  name: string;
  inception?: Object | null | String;
  status: String | Number | null;
};
export default class Worker extends Component<IWorker, State> {
  state: State = {
    name: "",
    inception: "",
    status: null,
  };

  apiQuery = firestore
    .collection("WORKERS")
    .doc(this.props.instrument)
    .collection(this.props.timeFrame)
    .doc("DATA");

  componentDidMount() {

    this.apiQuery.get().then((res) => {
      console.log(res.data(), 'response')
      this.setState(() => ({
        name: res.data()?.name,
        inception: res.data()?.inception,
        status: res.data()?.status,
      }));
      console.log("didmount");
    });
  }

  componentDidUpdate(prevProps: any, prevState: any) {
    if (prevState.inception !== this.state.inception) {
      this.apiQuery.onSnapshot((snap) => {
        console.log(snap.data(), "status");

        this.setState(({
          status: snap.data()?.status,
        }));
        // console.log(snap.data()?.payload.date.toDate().toString(), 'kk')
      });

      // return () => this.unsubscribe()
    }
  }
  render() {
    return (
      <tr>
        <td>{this.state.name}</td>
        <td>{this.state.inception}</td>
        <td style={{ textAlign: "center" }}>
          {this.state.name ? <FcOk /> : <div>! <MdExplicit /></div>}
        </td>
      </tr>
    );
  }
}
