import React, { useState, useCallback, useMemo, useRef } from "react";
import {
  Formik,
  FormikHelpers,
  FormikProps,
  Form,
  Field,
  FieldProps,
} from "formik";



const Errors: React.FC<{}> = () => {
  // FORMIK CONST
  // const symbolValues: SymbolFormValues = { symbol: "" };

  const [symbol, setSymbol] = useState({ data: "" });
  const handleOnSubmit = (value: any, actions: any) => {
    // alert(JSON.stringify(values, null, 2));
    setSymbol({ data: value.symbol });
    console.log(symbol, "symbol");
    actions.setSubmitting(false);
  };

  // WSS CONST



  return (
    <div>
      <h1>Add Symbol WSS</h1>
      <Formik initialValues={symbol} onSubmit={handleOnSubmit}>
        <Form>
          <Field id="symbol" name="symbol" placeholder="Symbol" />
          <button type="submit">Submit</button>
        </Form>
      </Formik>
    </div>
  );
};
export default Errors;
