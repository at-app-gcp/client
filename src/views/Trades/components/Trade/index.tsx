import React, { useEffect, useState, Component } from "react";
import { firestore } from "../../../../routes/Firebase";
import { FcOk, FcHighPriority } from "react-icons/fc";
import { MdExplicit } from 'react-icons/md'
interface ITrade {
  tradeID: any
}

type State = {
  id: string,
  currentUnits: string,
  dividendAdjustment: string
  financing: string,
  initialMarginRequired: string,
  initialUnits: string,
  instrument: string,
  marginUsed: string,
  openTime: string,
  price: string,
  realizedPL: string,
  state: string,
  stopLoss: string,
  takeProfit: string
};
export default class Trade extends Component<ITrade, State> {
  state: State = {
    id: "",
    currentUnits: "",
    dividendAdjustment: "",
    financing: "",
    initialMarginRequired: "",
    initialUnits: "",
    instrument: "",
    marginUsed: "",
    openTime: "",
    price: "",
    realizedPL: "",
    state: "",
    stopLoss: "",
    takeProfit: ""
  };

  apiQuery = firestore
    .collection("TRADES")
    .doc(this.props.tradeID)

  componentDidMount() {


    this.apiQuery.get().then((res) => {
      console.log(res.data(), 'response')
      this.setState(() => ({
        id: res.data()?.trade.id,
        currentUnits: res.data()?.trade.currentUnits,
        dividendAdjustment: res.data()?.trade.dividendAdjustment,
        financing: res.data()?.trade.financing,
        initialMarginRequired: res.data()?.trade.initialMarginRequired,
        initialUnits: res.data()?.trade.initialUnits,
        instrument: res.data()?.trade.instrument,
        marginUsed: res.data()?.trade.marginUsed,
        openTime: res.data()?.trade.openTime,
        price: res.data()?.trade.price,
        realizedPL: res.data()?.trade.realizedPL,
        state: res.data()?.trade.state,
        stopLoss: res.data()?.trade.stopLossOrder.price,
        takeProfit: res.data()?.trade.takeProfitOrder.price
      }));
      console.log("didmount");
    });
  }

  componentDidUpdate(prevProps: any, prevState: any) {
    console.log("componentDidUpdate: ", prevProps, prevState.date);
    if (prevState.realizedPL !== this.state.realizedPL) {
      this.apiQuery.onSnapshot((res) => {
        this.setState(() => ({
          realizedPL: res.data()?.trade.realizedPL,
          state: res.data()?.trade.state,
        }));
      });
    }
  }
  render() {
    return (
      <tr>
        <td>{this.state.id}</td>
        <td>{this.state.realizedPL}</td>
        <td>{this.state.state}</td>
        <td>{this.state.initialUnits}</td>
        <td>{this.state.openTime}</td>
        <td>{this.state.instrument}</td>
        <td>{this.state.marginUsed}</td>
        <td>{this.state.stopLoss}</td>
        <td>{this.state.takeProfit}</td>
        <td>{this.state.price}</td>
      </tr>
    );
  }
}
