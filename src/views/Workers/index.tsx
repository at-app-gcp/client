import React, { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import { firestore } from "../../routes/Firebase";
import Worker from "./components/Worker";
import { Form, Button, Card, Alert, Container, Spinner } from "react-bootstrap";
export default function Workers() {
  const [workersNames, setWorkersNames] = useState<any>([]);

  useEffect(() => {
    const un = firestore
      .collection("SYSTEM")
      .doc("broker")
      .get()
      .then((res) => {
        console.log(res.data()?.payload, "payload");
        const payload = res.data();
        const instruments = payload?.instruments;
        const time_frames = payload?.time_frames;

        const instances: any = [];
        instruments.forEach((e: any) => {
          time_frames.forEach(function (t: string, i: any) {
            instances.push({ instrument: e, timeFrame: t });
          });
        });
        setWorkersNames(instances);
      });
  }, []);

  return (
    <Table striped bordered hover size="sm" responsive="sm" variant="dark">
      <thead>
        <tr>
          <th className="not_bold">Worker</th>
          <th className="not_bold">Inception</th>
          <th className="not_bold" style={{ textAlign: "center" }}>Status</th>
        </tr>
      </thead>
      <tbody>

        {workersNames.length === 0 ? (
          <div style={{ textAlign: "center" }}>
            <Spinner animation="border" as={"p"} />
          </div>
        ) : (
            workersNames.map((e: any, i: any) => {
              return (
                <Worker
                  key={i}
                  instrument={e.instrument}
                  timeFrame={e.timeFrame}
                ></Worker>
              );
            })
          )}
      </tbody>
    </Table>
  );
}
