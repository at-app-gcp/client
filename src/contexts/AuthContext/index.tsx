import React, { useContext, useState, useEffect } from "react"
import { auth } from '../../routes/Firebase'
import firebase from 'firebase/app'

type ContextProps = { 
    authenticated: boolean,
    lang: string,
    theme: string,
    value : any,
    currentUser:any,
    user: any,
    logout: any
  };



const AuthContext = React.createContext<Partial<ContextProps>>({})

export function useAuth() {
  return useContext(AuthContext)
}

export function AuthProvider({ children }:any) {
  const [currentUser, setCurrentUser] = useState<any>([])
  const [loading, setLoading] = useState(true)

  function signup(email:any, password:any) {
    return auth.createUserWithEmailAndPassword(email, password)
  }

  function login(email:any, password:any) {
   
      return  auth.signInWithEmailAndPassword(email, password)

    
  }

  function logout(e:any) {
    
    return auth.signOut()
  }

  function resetPassword(email:any) {
    return auth.sendPasswordResetEmail(email)
  }

  async function updateEmail(email:any) {
      
    return await currentUser.updateEmail(email)
  }

  function updatePassword(password:any) {
    return currentUser.updatePassword(password)
  }

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((user):any => {
      setCurrentUser(user)
      setLoading(false)
    })

    return unsubscribe
  }, [])

  type AppContextInterface = {
currentUser :any,
login : any
signup : any
logout :any
resetPassword :any
updateEmail:any
updatePassword:any

  }


  const sampleAppContext: AppContextInterface = {
    currentUser,
   login,
   signup,
   logout,
   resetPassword,
   updateEmail,
   updatePassword
  };

  return (
    <AuthContext.Provider value={sampleAppContext}>
      {!loading && children}
    </AuthContext.Provider>
  )
}
