import firebase from "firebase/app"
import "firebase/auth"
import "firebase/firestore"

const app = firebase.initializeApp({
  apiKey: "AIzaSyDbzc0V81WdUlU-kmM7zMS2YRh-1u2lmEM",
  authDomain: "at202xcloud.firebaseapp.com",
  databaseURL: "https://at202xcloud.firebaseio.com",
  projectId: "at202xcloud",
  storageBucket: "at202xcloud.appspot.com",
  messagingSenderId: "421087735296",
  appId: "1:421087735296:web:f75289d65c20cfe19672e5"
})

firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION);

export const firestore = app.firestore()
export const auth = app.auth()
export default app
