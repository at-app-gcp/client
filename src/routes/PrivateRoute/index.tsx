import React from "react"
import { Route, Redirect, RouteProps} from "react-router-dom"
import { useAuth } from "../../contexts/AuthContext"

interface PrivateRouteProps extends Omit<RouteProps, "component"> {
    component: React.ElementType;
  }
const PrivateRoute: React.FC<PrivateRouteProps> = ({ component: Component, ...rest }) => {
    const { currentUser } = useAuth()

      return (
        <Route
          {...rest}
          render={props => {
            return currentUser ? <Component {...props} /> : <Redirect to="/login" />
          }}
        ></Route>
      )
  }

export default PrivateRoute
 