import React from "react";

import Home from "../../views/Home";
import Trades from "../../views/Trades";
import Workers from "../../views/Workers";
import Settings from "../../views/Settings"
import Errors from "../../views/Errors"
import Crypto from "../../views/Crypto"

import { FaHome, FaChartLine, FaMoneyCheckAlt, FaUserCircle} from 'react-icons/fa';
import { MdSettings, MdError } from 'react-icons/md';
import { FaRobot } from 'react-icons/fa';

const routes = [

  {
    path: "/home",
    exact : true,
    name: "Home",
    component: () => <Home />,
    icon:  <FaHome/>
  },
  {
    path: "/trades",
    name: "Trades",
    component: () => <Trades />,
    icon: <FaChartLine/>,
  },
  {
    path: "/workers",
    name: "Workers",
    component: () => <Workers />,
    icon : <FaRobot/>
  },
  {
    path: "/errors",
    name: "Errors",
    component: () => <Errors />,
    icon : <MdError/>
  },
  {
    path: "/crypto",
    name: "Crypto",
    component: () => <Crypto />,
    icon : null
  },
  {
    path: "/settings",
    name: "Settings",
    component: () => <Settings />,
    icon : <MdSettings/>
  },
];

export default routes;
