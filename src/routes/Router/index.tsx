import React, { useRef } from "react";
import { Switch, Route, NavLink } from "react-router-dom";
import routes from "./routes";
import {
  Container,
  Navbar,
  Nav,
  NavDropdown,
  Button,
  ListGroup,
} from "react-bootstrap";
import CustomDiv from "./CustomDiv";
// import { Switch, Route, Link, useHistory } from "react-router-dom";
// import { useAuth } from "../../../contexts/AuthContext";
import { FaUserCircle } from "react-icons/fa";
import { isMobile } from "react-device-detect";

export default function Router() {
  let [selected, setSelected] = React.useState("white");
  const alfa = useRef<HTMLInputElement | null | string | any>(null);
  const screenWidth = window.innerWidth;
  const valScreen = screenWidth / routes.length;

  return (
    <>
      {isMobile ? (
        <Navbar fixed="bottom" style={{ margin: "0px 0px 0px 0px", padding: "0px" }}>
          <ListGroup horizontal as="ul">
            {routes.map((route, index) => (
              <NavLink
                key={index + 2000}
                to={route.path}
                style={{
                  color: "black",
                  textDecoration: "none",
                  width: valScreen,
                }}
                activeStyle={{
                  color: "black",
                  borderTop: "2px inset  lightgrey",
                }}
              >
                <div
                  style={{
                    textAlign: "center",
                    textDecoration: "none",
                    backgroundColor: "black",
                    // #2f3034
                    color: "#cecece",
                  }}
                >
                  <div>{route.icon}</div>
                  <div style={{ fontFamily: "roboto" }}>{route.name}</div>
                </div>
              </NavLink>
            ))}
          </ListGroup>
        </Navbar>
      ) : (
        <Navbar style={{ margin: "0px 0px 0px 0px", padding: "0px" }}>
          <ListGroup horizontal as="ul">
            {routes.map((route, index) => (
              <NavLink
                key={index + 2000}
                to={route.path}
                style={{
                  color: "black",
                  textDecoration: "none",
                  width: valScreen,
                }}
                activeStyle={{
                  color: "black",
                  borderBottom: "2px inset  lightgrey",
                }}
              >
                <div
                  style={{
                    textAlign: "center",
                    textDecoration: "none",
                    backgroundColor: "#2f3034",
                    color: "#cecece",
                  }}
                >
                  <div>{route.icon}</div>
                  <div style={{ fontFamily: "roboto" }}>{route.name}</div>
                </div>
              </NavLink>
            ))}
          </ListGroup>
        </Navbar>
      )}

      {/* CONTENT */}

      <Switch>
        {routes.map((route, index) => (
          <Route
            key={index + 3000}
            path={route.path}
            exact={route.exact}
            children={<route.component />}
          />
        ))}
      </Switch>
    </>
  );
}
// {/* <CustomDiv data={[route.icon,route.name]} myRef = {alfa}>
//     {/* {route.name} */}
//     </CustomDiv>
//   */}
// <div style={{ display: "flex", height: window.innerHeight - 60 + "px" }}>
//         <div style={{ boxShadow: "0px 0px 0px 2px rgba(0, 0, 0, 0.1)" }}>
//           <ListGroup as="ul">
//             {routes.map((route, index) => (
//               <ListGroup.Item as="li" key={index + 1000}>
//                 <NavLink
//                   key={index + 2000}
//                   to={route.path}
//                   style={{ color: "black" }}
//                   activeStyle={{ color: "#007aff" }}
//                 >
//                   {route.icon}
//                 </NavLink>
//               </ListGroup.Item>
//             ))}
//           </ListGroup>
//         </div>
//         <div style={{ width: "100vw", padding: "1vh" }}>
//           <Switch>
//             {routes.map((route, index) => (
//               <Route
//                 key={index + 3000}
//                 path={route.path}
//                 exact={route.exact}
//                 children={<route.component />}
//               />
//             ))}
//           </Switch>
//         </div>
//       </div>
