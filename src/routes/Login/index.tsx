import React, { useRef, useState } from "react";
import { Form, Button, Card, Alert, Container, Spinner } from "react-bootstrap";
import { useAuth } from "../../contexts/AuthContext";
import { Link, useHistory } from "react-router-dom";

export default function Login() {
  const emailRef = useRef<any>([]);
  const passwordRef = useRef<any>([]);
  const { login }: any = useAuth();
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  async function handleSubmit(e: any) {
    e.preventDefault();

    try {
      setError("");
      setLoading(true);
      await login(emailRef.current.value, passwordRef.current.value);
      history.push("/home");
    } catch {
      setError("Incorrect e-mail or password");
    }

    setLoading(false);
  }

  return (
    <Container
      className="d-flex align-items-center justify-content-center "
      style={{ minHeight: "100vh" , backgroundColor : "#33383e" }}
    >
      <div className="w-100" style={{ maxWidth: "400px"}}>
        <Card border="secondary">
          <Card.Body className="bg-dark">
            <h2 className="text-center mb-4">Log In</h2>
            {error && <Alert variant="danger">{error}</Alert>}
            {loading? <div style={{textAlign : "center"}}><Spinner animation="border" as={"p"} /></div> : <div style={{height :"32px"}} ></div>}
            {/* {loading?  <div style={{height : '32px'}} ></div> : <div style={{textAlign : "center"}}><Spinner animation="border" as={"p"} /></div>} */}
            <Form onSubmit={handleSubmit}>
              <Form.Group id="email">
                <Form.Label>Email</Form.Label>
                <Form.Control type="email" ref={emailRef} required />
              </Form.Group>
              <Form.Group id="password">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" ref={passwordRef} required />
              </Form.Group>
              <Button disabled={loading} className="w-100" type="submit" variant="success">
                Submit
              </Button>
            </Form>
          </Card.Body>
        </Card>
      </div>
    </Container>
  );
}
