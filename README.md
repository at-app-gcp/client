Solution
You can declare let isMounted = true inside useEffect, which will be changed in the cleanup callback, as soon as the component is unmounted. Before state updates, you now check this variable conditionally:

useEffect(() => {
  let isMounted = true; // note this flag denote mount status
  someAsyncOperation().then(data => {
    if (isMounted) setState(data);
  })
  return () => { isMounted = false }; // use effect cleanup to set flag false, if unmounted
});# AT2020React




# https://www.robotstxt.org/robotstxt.html
User-agent: *
Disallow:

https://help.darwinex.com/position-vs-trade



//   // useEffect(() => {
//   //   const unsubscribe = firestore
//   //     .collection("WORKERS")
//   //     .doc("EUR_AUD")
//   //     .collection("M1")
//   //     .doc("DATA")
//   //     .onSnapshot((snapshot) => {
//   //       setSignalDetails(snapshot.data());
//   //       console.log(signalDetails.data, "dasd");
//   //     });
//   //   return () => unsubscribe();
//   // }, []);

//   useEffect(() => {
//     // const unsubscribe = firestore.collection("WORKERS").onSnapshot((snap) => {
//     //   console.log(snap)
//     // })
//     const unsubscribe = firestore.collection('WORKERS').get()
//     unsubscribe.then((snap) => {
//       console.log(snap.query)
//     })
//     // return () => unsubscribe();
//   }, []);

  // useEffect(() => {
  //     const uns = firestore
  //     .collection("WORKERS")
  //     .onSnapshot((querySnapshot) => {
  //         querySnapshot.forEach((doc) => {
  //             console.log(doc.data()); // For data inside doc
  //             console.log(doc.id); // For doc name
  //         })
  //     }
  // },[])

  // useEffect(() => {
  //   firestore.collection("WORKERS").onSnapshot((snap) => {
  //     console.log(snap.empty)
  //     snap.forEach((doc) => {
  //       console.log(doc);
  //     });
  //   });
  // });


  // const Worker = (props: IWorker) => {

//   console.log(props, "props");
//   const [signalDetails, setSignalDetails] = useState<any>([]);
//   useEffect(() => {
//     const unsubscribe = firestore
//       .collection("WORKERS")
//       .doc(props.instrument)
//       .collection(props.timeFrame)
//       .doc("DATA")
//       .onSnapshot((snapshot) => {
//         // return setSignalDetails(snapshot.data());
//         // console.log(snapshot.data(), "dasd");
//         return (
//           <div>
//             {snapshot.data()?.payload.instrument}
//           </div>
//         )
//       });

//     return () => unsubscribe();
//   }, []);
//   const alf = () => {
//     const g = signalDetails.payload
//     return (
//       <div>
//         {g.instrument}
//       </div>
//     )
//   }
//   return (
//     <div>

//     {signalDetails ?
//     (<ul> <li>{alf()}asdaqd</li>
//     {/* <li>{signalDetails && signalDetails.payload.name}</li>
//     <li>{signalDetails && signalDetails.payload.time_frame}</li>     */}
//     {/* <li>{signalDetails && signalDetails.payload.uid}sda</li> */}

//     </ul>)
//     // (<div>{console.log(signalDetails)}sadasdasdasd</div>)

//     : <p>jjjjj</p>}

//     </div>
//   );
// };

// export default Worker;



TERST Sat Dec 12 2020 17:12:27 GMT+0100 (Central European Standard Time)
Reads
16,634
First 50,000 free.
Writes
8,774
First 20,000 free.
Deletes
0
First 20,000 free.






// export default function Trades() {
//   const [trades, setTrades]: any = React.useState<any>([]);
//   // console.log(apiQuery)

//   React.useEffect(():any => {
//     const q = apiQuery.get().then((res) => {
//       // console.log(res.docs)
//       setTrades(res.docs)
//       // setState({data : e.data()})
//     });
//     return () => q
//   }, []);


//   // React.useEffect(():any => {
//   //   if (trades.length !==)
//   //   const q = apiQuery.get().then((res) => {
//   //     // console.log(res.docs)
//   //     setTrades(res.docs)
//   //     // setState({data : e.data()})
//   //   });
//   //   return () => q
//   // }, []);
//   // React.useEffect(() => {
//   //   if (prevState.realizedPL !== this.state.realizedPL) {
//   //     this.apiQuery.onSnapshot((res) => {
//   //       this.setState(() => ({
//   //         realizedPL : res.data()?.trade.realizedPL,
//   //         state : res.data()?.trade.state,
//   //       }));
//   //     });

//   //   //   // return () => this.unsubscribe()
//   //   }
//   // }, []);


//   console.log(typeof trades, 'tradessssssss')
//   return (
//     <div>

//         {trades.length > 0 ? trades.map((e: any, i: any, s: any) => 
//         (<Trade tradeID ={e.data().trade.id}/>)) 
//         : 
//         (<div style={{ textAlign: "center" }}><Spinner animation="border" as={"p"} /></div>)}


//     </div>
//   )

//       }


    // <div>
    //   {workersNames == [] ? (
    //     <p>alfa</p>
    //   ) : (
    //     workersNames.map((e: any, i: any) => {
    //       return (

    //         <Worker
    //           key={i}
    //           instrument={e.instrument}
    //           timeFrame={e.timeFrame}
    //         ></Worker>
    //       );
    //     })
    //   )}
    // </div>
